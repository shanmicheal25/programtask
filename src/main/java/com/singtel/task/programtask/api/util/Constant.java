package com.singtel.task.programtask.api.util;

public class Constant {

	public static final String I_AM_WALKING = "I am walking";
	public static final String I_AM_FLYING = "I am flying";
	public static final String I_AM_NOT_FLYING = "I am not fly";
	public static final String I_CAN_SWIM = "I can swim";
	public static final String I_MAKE_SPECIAL_SOUND = "I make Special Sound";
	public static final String I_AM_NOT_MAKE_SOUND = "I am not make Sound";
	public static final String I_EAT_FISH = "I eat fish";
	public static final String I_MAKE_JOKES = "I make jokes";
	public static final String I_CANT_FLY = "I cant Fly";
	public static final String I_CANT_SING = "I cant Sing";
	public static final String I_CANT_WALK = "I cant Walk";

	// special sounds .
	public static final String DUCKS_SOUND = "Quack, quack";
    public static final String CHICKENS_SOUND = "Cluck, cluck";
    public static final String ROOSTERS_SOUND = "Cock-a-doodle-doo";
    public static final String DOGS_SOUND = "Woof, woof";
    public static final String CATS_SOUND = "Meow, meow";
    public static final String DEFAULT_SOUND = "Silent";
}
