package com.singtel.task.programtask;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.StringContains.containsString;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.TestInfo;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.singtel.task.programtask.api.nature.IParrot;
import com.singtel.task.programtask.api.pojo.Animal;
import com.singtel.task.programtask.api.pojo.Bird;
import com.singtel.task.programtask.api.pojo.Butterfly;
import com.singtel.task.programtask.api.pojo.Cat;
import com.singtel.task.programtask.api.pojo.Chicken;
import com.singtel.task.programtask.api.pojo.Clownfish;
import com.singtel.task.programtask.api.pojo.Dog;
import com.singtel.task.programtask.api.pojo.Dolphin;
import com.singtel.task.programtask.api.pojo.Duck;
import com.singtel.task.programtask.api.pojo.Fish;
import com.singtel.task.programtask.api.pojo.Parrot;
import com.singtel.task.programtask.api.pojo.Rooster;
import com.singtel.task.programtask.api.pojo.Shark;
import com.singtel.task.programtask.api.service.AnimalService;
import com.singtel.task.programtask.api.util.Constant;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProgramtaskApplicationTests {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProgramtaskApplicationTests.class);

	private ByteArrayOutputStream outSpy = new ByteArrayOutputStream();

	@Autowired
	AnimalService service;
	
	@BeforeAll
	public static void setUp() {
		System.out.println("@BeforAll - Executes");
	}

	@BeforeEach
	void init(TestInfo testInfo) {
		System.out.println("Start..." + testInfo.getDisplayName());
	}

	@AfterEach
	void tearDown(TestInfo testInfo) {
		System.out.println("Finished..." + testInfo.getDisplayName());
	}

	@Test
	@DisplayName("Add Test Birds")
	public void testBirds() {

		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Bird().walk();
		assertThat(outSpy.toString(), containsString("I am walking"));

		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Bird().fly();
		assertThat(outSpy.toString(), containsString("I am flying"));

		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Bird().sing();
		assertThat(outSpy.toString(), containsString("I am singing"));

	}

	@Test
	@DisplayName("Test Special Kinds of birds")
	public void testSpecialBirds() {
		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Duck().sing();
		assertThat(outSpy.toString(), containsString(Constant.I_CANT_SING));

		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Duck().walk();
		assertThat(outSpy.toString(), containsString(Constant.I_AM_WALKING));

		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Duck().specialSound();
		assertThat(outSpy.toString(), containsString(Constant.DUCKS_SOUND));

		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Chicken().specialSound();
		assertThat(outSpy.toString(), containsString(Constant.CHICKENS_SOUND));

		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Chicken().fly();
		assertThat(outSpy.toString(), containsString(Constant.I_CANT_FLY));

	}

	@Test
	@DisplayName("Test Model Rooster ")
	public void testRooster() {
		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Rooster().specialSound();
		assertThat(outSpy.toString(), containsString(Constant.ROOSTERS_SOUND));

		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Rooster().fly();
		assertThat(outSpy.toString(), containsString(Constant.I_CANT_FLY));

	}

	@Test
	@DisplayName("Test Model Parrot ")
	public void testParrot() {
		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		IParrot dog = new Dog();
		dog.specialSound();
		assertThat(outSpy.toString(), containsString(Constant.DOGS_SOUND));

		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		IParrot cat = new Cat();
		cat.specialSound();
		assertThat(outSpy.toString(), containsString(Constant.CATS_SOUND));

		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		IParrot rooster = new Rooster();
		rooster.specialSound();
		assertThat(outSpy.toString(), containsString(Constant.ROOSTERS_SOUND));

	}

	@Test
	@DisplayName("Test Model B Fish")
	public void testFishes() {
		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Fish().walk();
		assertThat(outSpy.toString(), containsString(Constant.I_CANT_WALK));
		
		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Fish().sing();
		assertThat(outSpy.toString(), containsString(Constant.I_CANT_SING));
		
		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Fish().swim();
		assertThat(outSpy.toString(), containsString(Constant.I_CAN_SWIM));
		
		
	}
	
	@Test
	@DisplayName("Test Model B Shark, clownfish & dolphin")
	public void testSharkClownFish() {
		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Shark().color();
		assertThat(outSpy.toString(), containsString("grey"));
		
		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Clownfish().color();
		assertThat(outSpy.toString(), containsString("colourful(Orange)"));
		
		
		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Dolphin().swim();
		assertThat(outSpy.toString(), containsString(Constant.I_CAN_SWIM));
		
	}
	
	
	@Test
	@DisplayName("Test Model D Butterfly and Caterpillar")
	public void testButterflyCaterpillar() {
		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Butterfly().fly();
		assertThat(outSpy.toString(), containsString(Constant.I_AM_FLYING));
		
		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Butterfly().specialSound();
		assertThat(outSpy.toString(), containsString(Constant.I_AM_NOT_MAKE_SOUND));
		
		
		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Butterfly.Caterpillar().fly();
		assertThat(outSpy.toString(), containsString(Constant.I_AM_NOT_FLYING));
		
		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		new Butterfly.Caterpillar().walk();;
		assertThat(outSpy.toString(), containsString(Constant.I_AM_WALKING));
		
		
	}
	
	@Test
	@DisplayName("Test the count of the Animals ")
	public void testAnimalCategoryCount() {
		
		
		
		outSpy = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outSpy));
		Animal[] animals = new Animal[] { new Bird(), new Duck(), new Chicken(), new Rooster(), new Parrot(),
				new Fish(), new Shark(), new Clownfish(), new Dolphin(), new Dog(), new Butterfly(), new Cat(), new Butterfly.Caterpillar() };

		service.calculateCount(animals);
		assertThat(outSpy.toString(), containsString("flyingAnimal : 5 -  walkingAnimal : 5   -  singingAnimal : 7   - swimmingAnimal :  5   - notFlyingAnimal : 3"));
		
		
	}
}
