package com.singtel.task.programtask.api.pojo;

import com.singtel.task.programtask.api.nature.NotFlyable;
import com.singtel.task.programtask.api.nature.Soundable;
import com.singtel.task.programtask.api.util.Constant;

public class Chicken extends Bird implements NotFlyable, Soundable {

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println(Constant.I_CANT_FLY);
	}

	@Override
	public void specialSound() {
		// TODO Auto-generated method stub
		System.out.println(Constant.CHICKENS_SOUND);
	}
	
	

}
