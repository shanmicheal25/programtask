package com.singtel.task.programtask.api.nature;

public interface NotFlyable {

	void fly();
}
