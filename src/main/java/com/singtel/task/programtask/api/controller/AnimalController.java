package com.singtel.task.programtask.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.singtel.task.programtask.api.pojo.Animal;
import com.singtel.task.programtask.api.pojo.Bird;
import com.singtel.task.programtask.api.pojo.Butterfly;
import com.singtel.task.programtask.api.pojo.Cat;
import com.singtel.task.programtask.api.pojo.CategoryCount;
import com.singtel.task.programtask.api.pojo.Chicken;
import com.singtel.task.programtask.api.pojo.Clownfish;
import com.singtel.task.programtask.api.pojo.Dog;
import com.singtel.task.programtask.api.pojo.Dolphin;
import com.singtel.task.programtask.api.pojo.Duck;
import com.singtel.task.programtask.api.pojo.Fish;
import com.singtel.task.programtask.api.pojo.Parrot;
import com.singtel.task.programtask.api.pojo.Rooster;
import com.singtel.task.programtask.api.pojo.Shark;
import com.singtel.task.programtask.api.service.AnimalService;

@RestController
public class AnimalController {

	@Autowired
	AnimalService animalService;
	
	Animal[] animals = new Animal[] { new Bird(), new Duck(), new Chicken(), new Rooster(), new Parrot(),
			new Fish(), new Shark(), new Clownfish(), new Dolphin(), new Dog(), new Butterfly(), new Cat(), new Butterfly.Caterpillar() };


	@RequestMapping(method = RequestMethod.GET, value = "/animal/getAnimalCategoryCount")
	@ResponseBody
	public CategoryCount getFlyingAnimalCount() {
		
		CategoryCount catCount = animalService.calculateCount(animals);
		
		return catCount;
	}
	
}
