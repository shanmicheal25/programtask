package com.singtel.task.programtask;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgramtaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProgramtaskApplication.class, args);
		
	}

}
