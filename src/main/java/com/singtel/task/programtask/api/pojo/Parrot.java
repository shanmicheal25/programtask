package com.singtel.task.programtask.api.pojo;

import com.singtel.task.programtask.api.nature.IParrot;
import com.singtel.task.programtask.api.util.Constant;

public class Parrot extends Animal implements IParrot {

	@Override
	public void specialSound() {
		// TODO Auto-generated method stub
		System.out.println(Constant.DEFAULT_SOUND);
	}

}
