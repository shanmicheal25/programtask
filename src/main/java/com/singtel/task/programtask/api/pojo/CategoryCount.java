package com.singtel.task.programtask.api.pojo;

public class CategoryCount {

	int flyingAnimal = 0;
	int walkingAnimal = 0;
	int singingAnimal = 0;
	int swimmingAnimal = 0;
	int notFlyingAnimal = 0;

	public int getFlyingAnimal() {
		return flyingAnimal;
	}

	public void setFlyingAnimal(int flyingAnimal) {
		this.flyingAnimal = flyingAnimal;
	}

	public int getWalkingAnimal() {
		return walkingAnimal;
	}

	public void setWalkingAnimal(int walkingAnimal) {
		this.walkingAnimal = walkingAnimal;
	}

	public int getSingingAnimal() {
		return singingAnimal;
	}

	public void setSingingAnimal(int singingAnimal) {
		this.singingAnimal = singingAnimal;
	}

	public int getSwimmingAnimal() {
		return swimmingAnimal;
	}

	public void setSwimmingAnimal(int swimmingAnimal) {
		this.swimmingAnimal = swimmingAnimal;
	}

	public int getNotFlyingAnimal() {
		return notFlyingAnimal;
	}

	public void setNotFlyingAnimal(int notFlyingAnimal) {
		this.notFlyingAnimal = notFlyingAnimal;
	}

}
