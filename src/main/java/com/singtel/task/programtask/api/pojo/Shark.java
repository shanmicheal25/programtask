package com.singtel.task.programtask.api.pojo;

import com.singtel.task.programtask.api.util.Constant;

public class Shark extends Fish {

	@Override
	public void size() {
		System.out.println("large");
	}

	@Override
	public void color() {
		System.out.println("grey");
	}

	public void eat() {
		System.out.println(Constant.I_EAT_FISH);
	}
}
