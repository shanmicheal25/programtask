package com.singtel.task.programtask.api.pojo;

import com.singtel.task.programtask.api.nature.Flyable;
import com.singtel.task.programtask.api.nature.Singable;
import com.singtel.task.programtask.api.nature.Walkable;

public class Bird extends Animal implements Singable, Walkable, Flyable {
	
	@Override
	public void fly() {
		System.out.println("I am flying");
	}

	@Override
	public void walk() {
		System.out.println("I am walking");
	}
	
	@Override
	public void sing() {
		System.out.println("I am singing");
	}
}