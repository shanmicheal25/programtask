package com.singtel.task.programtask.api.pojo;

import com.singtel.task.programtask.api.nature.IParrot;
import com.singtel.task.programtask.api.util.Constant;

public class Dog extends Animal implements IParrot  {

	@Override
	public void specialSound() {

		System.out.println(Constant.DOGS_SOUND);
	}

}
