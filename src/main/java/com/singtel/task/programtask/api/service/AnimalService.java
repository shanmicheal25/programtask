package com.singtel.task.programtask.api.service;

import org.springframework.stereotype.Service;

import com.singtel.task.programtask.api.nature.Flyable;
import com.singtel.task.programtask.api.nature.NotFlyable;
import com.singtel.task.programtask.api.nature.Singable;
import com.singtel.task.programtask.api.nature.Swimmable;
import com.singtel.task.programtask.api.nature.Walkable;
import com.singtel.task.programtask.api.pojo.Animal;
import com.singtel.task.programtask.api.pojo.Bird;
import com.singtel.task.programtask.api.pojo.Butterfly;
import com.singtel.task.programtask.api.pojo.Cat;
import com.singtel.task.programtask.api.pojo.CategoryCount;
import com.singtel.task.programtask.api.pojo.Chicken;
import com.singtel.task.programtask.api.pojo.Clownfish;
import com.singtel.task.programtask.api.pojo.Dog;
import com.singtel.task.programtask.api.pojo.Dolphin;
import com.singtel.task.programtask.api.pojo.Duck;
import com.singtel.task.programtask.api.pojo.Fish;
import com.singtel.task.programtask.api.pojo.Parrot;
import com.singtel.task.programtask.api.pojo.Rooster;
import com.singtel.task.programtask.api.pojo.Shark;

@Service
public class AnimalService {

	Animal[] animals;
	
	CategoryCount categoryCount;


	public CategoryCount calculateCount(Animal[] animals) {
		this.animals = animals;
		categoryCount = new CategoryCount();
		
		int flyingAnimal = 0;
		int walkingAnimal = 0;
		int singingAnimal = 0;
		int swimmingAnimal = 0;
		int notFlyingAnimal = 0;
		
		for (int i = 0; i < animals.length; i++) {
			if (animals[i] instanceof Flyable) {
				flyingAnimal += 1;
			}
			if (animals[i] instanceof Walkable) {
				walkingAnimal += 1;
			}
			if (animals[i] instanceof Singable) {
				singingAnimal += 1;
			}
			if (animals[i] instanceof Swimmable) {
				swimmingAnimal += 1;
			}
			if (animals[i] instanceof NotFlyable) {
				notFlyingAnimal += 1;
			}
		}
		
		categoryCount.setFlyingAnimal(flyingAnimal);
		categoryCount.setNotFlyingAnimal(notFlyingAnimal);
		categoryCount.setSingingAnimal(singingAnimal);
		categoryCount.setSwimmingAnimal(swimmingAnimal);
		categoryCount.setWalkingAnimal(walkingAnimal);

		System.out.println("flyingAnimal : " + flyingAnimal + " -  walkingAnimal : " + walkingAnimal
				+ "   -  singingAnimal : " + singingAnimal + "   - swimmingAnimal :  " + swimmingAnimal+ "   - notFlyingAnimal : "+notFlyingAnimal);
		
		
		return categoryCount;
	}

	
	public static void main(String arg[]) {

		Animal[] animals = new Animal[] { new Bird(), new Duck(), new Chicken(), new Rooster(), new Parrot(),
				new Fish(), new Shark(), new Clownfish(), new Dolphin(), new Dog(), new Butterfly(), new Cat(),
				new Butterfly.Caterpillar() };

		AnimalService service = new AnimalService();

		service.calculateCount(animals);

		System.out.println(service.toString());
	}
}
