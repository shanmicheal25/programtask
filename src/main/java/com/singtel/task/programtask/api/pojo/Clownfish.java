package com.singtel.task.programtask.api.pojo;

import com.singtel.task.programtask.api.util.Constant;

public class Clownfish extends Fish{

	@Override
	public void size() {
		System.out.println("small");
	}

	@Override
	public void color() {
		System.out.println("colourful(Orange)");
	}

	public void eat() {
		System.out.println(Constant.I_MAKE_JOKES);
	}
}
