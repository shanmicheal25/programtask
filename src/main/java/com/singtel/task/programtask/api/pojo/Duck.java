package com.singtel.task.programtask.api.pojo;

import com.singtel.task.programtask.api.nature.Soundable;
import com.singtel.task.programtask.api.nature.Swimmable;
import com.singtel.task.programtask.api.util.Constant;

public class Duck extends Bird implements Swimmable, Soundable{

	@Override
	public void swim() {
		// TODO Auto-generated method stub
		System.out.println(Constant.I_CAN_SWIM);
	}

	@Override
	public void specialSound() {
		// TODO Auto-generated method stub
		System.out.println(Constant.DUCKS_SOUND);
		
	}
	
	@Override
	public void sing() {
		System.out.println(Constant.I_CANT_SING);
	}

}
