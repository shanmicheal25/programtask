package com.singtel.task.programtask.api.nature;

public interface Swimmable {

	void swim();
}
