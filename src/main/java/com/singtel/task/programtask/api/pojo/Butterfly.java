package com.singtel.task.programtask.api.pojo;

import com.singtel.task.programtask.api.nature.Flyable;
import com.singtel.task.programtask.api.nature.NotFlyable;
import com.singtel.task.programtask.api.nature.Soundable;
import com.singtel.task.programtask.api.nature.Walkable;
import com.singtel.task.programtask.api.util.Constant;

public class Butterfly extends Animal implements Flyable, Soundable {

	@Override
	public void fly() {
		// TODO Auto-generated method stub
		System.out.println(Constant.I_AM_FLYING);
	}

	@Override
	public void specialSound() {
		// TODO Auto-generated method stub
		System.out.println(Constant.I_AM_NOT_MAKE_SOUND);
	}

	
	public static class Caterpillar extends Animal implements NotFlyable, Walkable {

		@Override
		public void fly() {
			// TODO Auto-generated method stub
			
			System.out.println(Constant.I_AM_NOT_FLYING);
		}

		@Override
		public void walk() {
			// TODO Auto-generated method stub
			System.out.println(Constant.I_AM_WALKING);
		}
		
	}

}
