package com.singtel.task.programtask.api.nature;

public interface BodyType {

	void size();
	
	void color();
}
