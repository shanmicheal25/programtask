package com.singtel.task.programtask.api.pojo;

import com.singtel.task.programtask.api.nature.BodyType;
import com.singtel.task.programtask.api.nature.Singable;
import com.singtel.task.programtask.api.nature.Swimmable;
import com.singtel.task.programtask.api.util.Constant;

public class Fish extends Animal implements Swimmable, Singable, BodyType{

	
	public void walk() {
		System.out.println(Constant.I_CANT_WALK);
	}
	
	@Override
	public void sing() {
		System.out.println(Constant.I_CANT_SING);
	}
	
	@Override
	public void swim() {
		// TODO Auto-generated method stub
		System.out.println(Constant.I_CAN_SWIM);
	}

	@Override
	public void size() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void color() {
		// TODO Auto-generated method stub
		
	}
	
	
}
