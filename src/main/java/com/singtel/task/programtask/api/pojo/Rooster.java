package com.singtel.task.programtask.api.pojo;

import com.singtel.task.programtask.api.nature.IParrot;
import com.singtel.task.programtask.api.util.Constant;

public class Rooster extends Chicken implements IParrot  {

	@Override
	public void fly() {
		super.fly();
	}

	@Override
	public void specialSound() {
		// TODO Auto-generated method stub
		System.out.println(Constant.ROOSTERS_SOUND);
	}
}
